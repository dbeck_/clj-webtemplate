# Clojure Web-Template

## How to start

##### Either

#### REPL
```
(-main
  {:port <port> :host <host>}
  {:user "<db>" :password <pw> :db-port <db-port>})
```

##### OR

Add cfg.edn to resources folder and match the Schema defined in init ns. 
#### LEININGEN
```
lein run
```

### After initialization
#### IN REPL
```
(start)

(stop)
```

## Currently working

- Dependency injection
- Event based websocket with sente
- Database integration (pg, sql, ...)
- Webserver
- Swagger supported endpoints
- Frontend routing

### Development in progress. Everything will be subject to heavy changes