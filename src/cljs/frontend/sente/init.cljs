(ns frontend.sente.init
  (:require
    [frontend.sente.router :refer [handle-event]]
    [frontend.util :as util]
    [taoensso.timbre :as log]
    [taoensso.sente :as sente :refer (cb-success?)]
    [cljs.core.async :refer [<! >! put! close! chan]]
    [re-frame.core :as rf])
  (:require-macros
    [cljs.core.async.macros :refer [go go-loop]]))

;; Setup -------------------------------------------------------
(def csrf-token
  ;; Sente requires a csrf-token to be present in each request.
  (when-let [el (.getElementById js/document "sente-csrf-token")]
    (.getAttribute el "data-csrf-token")))

(let [{:keys [chsk ch-recv send-fn state]}
      (sente/make-channel-socket-client!
        "/chsk"
        csrf-token
        {:type   :auto
         :packer :edn})]

  (def chsk chsk)
  (def ch-chsk ch-recv)
  (def chsk-send! send-fn)
  (def chsk-state state))

(defn start-router! []
  (sente/start-client-chsk-router!
    ch-chsk
    handle-event))

(defn do-send-server [{:keys [event-id timeout args cb]}]
  (let [request (partial chsk-send! [event-id args] (or timeout 5000))]
    (if cb
      (request (fn [cb-reply]
                 (when cb-reply
                   (println "callback -> " cb-reply))))
      (request))))