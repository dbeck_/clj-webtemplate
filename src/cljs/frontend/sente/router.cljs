(ns frontend.sente.router
  (:require [re-frame.core :refer [dispatch]]))

;; Events -------------------------------------------------
(defn set-token [resp]
  (println "hello dear")
  (let [{:keys [?data]} resp]
    (dispatch [:local/store-token ?data])))

(defn track-sugguestions [{:keys [?data]}]
  (dispatch [:submit/received-sugguestions ?data]))

;; Sente --------------------------------------------------
(defmulti handle-event :id)

(defmethod handle-event :default []
  (println "Unmatched event?"))

;; Sente state
(defmethod handle-event :chsk/state [{:keys [?data]}]
  (if (= ?data {:first-open? true})
    (dispatch [:websocket/open true])))

;; Sente handshake
(defmethod handle-event :chsk/handshake [{:keys [?data]}]
  (let [[?uid] ?data]
    (dispatch [:websocket/ready [?uid]])))

;; Sente event handler for websocket responses. Will dispatch functions
;; according to event id.
(defmethod handle-event :chsk/recv [{:keys [?data]}]
  (let [[ev-id resp] ?data]
    (case ev-id
      :submit/req-token (set-token resp)
      :submit/send-form (track-sugguestions resp))))