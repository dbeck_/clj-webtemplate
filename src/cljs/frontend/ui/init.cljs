(ns frontend.ui.init
  (:require
    ;;Deps
    [bidi.bidi :as bd]
    [pushy.core :as ps]
    [re-frame.core :as rf]))

(def ^:private app-routes
  "application routes"
  ["/" {""      :index
        "about" :about}])

;;Routing
(defn set-page! [match]
  (rf/dispatch [:page match]))

(defn- parse-url [url]
  (bd/match-route app-routes url))

(defn url-for [key]
  (bd/path-for app-routes key))

(defn start-routing []
  (ps/start! (ps/pushy set-page! parse-url)) )
