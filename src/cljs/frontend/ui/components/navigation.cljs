(ns frontend.ui.components.navigation
  (:require [frontend.ui.init :as routes]))

(defn make-link [to]
  (when to
    {:href (routes/url-for to)}))

(defn make-nav [entries]
  [:div.navbar-fixed
   [:nav
    [:div.nav-wrapper.blue.sticky
     [:div.container
      [:a.brand-logo {:href "#"} "Playlister"]
      [:ul#nav.right
       (for [{:keys [attributes to label]} entries]
         ^{:key label}
         [:li [:a (merge attributes (make-link to))
               label]])]]]]])