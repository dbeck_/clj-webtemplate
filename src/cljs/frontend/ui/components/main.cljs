(ns frontend.ui.components.main
  (:require
    [frontend.ui.init :refer [set-page!]]
    [frontend.util :as util]
    [frontend.ui.components.navigation :as nav]
    [cljs-http.client :as client]
    [reagent.core :as rg]
    [re-frame.core :as rf]))

(defonce form (rg/atom {:artist nil :track nil :playlist nil}))

(defn Form []
  [:div
   [:div.input-field
    [:label {:for "icon_prefix"} "Artist"]
    [:input.validate
     {:type     "text"
      :onChange #(swap! form assoc :artist (-> % .-target .-value))}]]
   [:div.input-field
    [:label {:for "track"} "Track"]
    [:input.validate
     {:type     "text"
      :onChange #(swap! form assoc :track (-> % .-target .-value))}]]
   [:div.input-field
    [:label {:for "playlist"} "Playlist"]
    [:input.validate
     {:type     "text"
      :onChange #(swap! form assoc :playlist (-> % .-target .-value))}]]])

(defn Submit []
  [:div [:a.waves-effect.waves-light.btn-small
         {:type     "submit"
          :on-click #(rf/dispatch [:submit/search @form])}
         "search"]])

(defn Tracklist []
  (let [tracks (rf/subscribe [:playlist/tracks])]
    [:div.row
     (for [item @tracks]
       ^{:key item}
       [:div.col.xl3.l5.offset-l1.m6.offset-m3.s8.offset-s2 {:style {:margin-top "10px"}}
        [:a
         {:style {:color "black"}}
         (concat(take 25 (:name (first item))) "...")]
        [:img {:on-click (fn [ev] (rf/dispatch [:submit/add-to-playlist (:id (first item))]))
               :src (get (nth (:images (first item)) 1) "url")
               :style {:margin-top "15px"}}]])]))

(defn Page []
  (let [auth "http://0.0.0.0:3000/api/login?client_id=c42cfca4e8d74c8eb6e72812f968379a&redirect_uri=http://0.0.0.0:3000/callback&scope=playlist-modify-public"]
    (rg/create-class
      {:reagent-render      (fn []
                              [:div.container
                               {:id    "app-wrapper"
                                :style {:margin-top "100px"}}
                               [:div
                                [:div.row
                                 [:div.col.s8.offset-s2.m6.offset-m3.l6.offset-l3.xl6
                                  [Form]
                                  [Submit]]]
                                [:div
                                 [Tracklist]]]])
       :component-did-mount (fn []
                              (when-not (= "authorized" (util/get-item "auth"))
                                (do
                                  (util/set-item! "auth" "authorized")
                                  (set! (.. js/window -location -href) auth))))})))

(defn home []
  [:div.page
   [:div.nav
    (nav/make-nav
      [{:attributes {}
        :label      ""
        :to         :index}])]
   [:div.content
    [Page]]])

;;Views
(defmulti panels identity)

(defmethod panels :index [] [home])
(defmethod panels :default [] [home])

;;Root
(defn components []
  (let [active-panel (rf/subscribe [:page])]
    [:div (panels @active-panel)]))

(defn mount-root []
  (rg/render [components]
    (.getElementById js/document "app")))