(ns ^:figwheel-hooks frontend.core
  (:require
    ;; Deps
    [devtools.core :refer [install!]]
    [stylefy.core :as sl]
    [re-frame.core :as rf]
    ;; Imports
    [frontend.state.events]
    [frontend.ui.init :as in]
    [frontend.state.subscriptions]
    [frontend.ui.components.main :as ro]
    [frontend.sente.init :as st]))

;; Debugging necessities
(install!)
(enable-console-print!)

;; Init
(defn ^:export init []
  (rf/dispatch-sync [:initialize])
  (in/start-routing)
  (st/start-router!)
  (sl/init)
  (ro/mount-root))

;; Hot Reloading
(defn ^:after-load re-mount []
  (let [state (rf/subscribe [:state])]
    (rf/dispatch-sync [:initialize])
    (in/start-routing)
    (sl/init)
    (st/start-router!)
    (ro/mount-root)
    (println "[NEW APPLICATION STATE] => " @state)))

(defn ^:before-load print-state []
  (let [state (rf/subscribe [:state])]
    (println "[PREVIOUS APPLICATION STATE] => " @state)))