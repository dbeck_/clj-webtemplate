(ns frontend.state.subscriptions
  (:require
    [re-frame.core :as rf]))

;; Pre-load hook to print current application-state to terminal
(rf/reg-sub
  :state
  (fn [db _]
    db))

(rf/reg-sub
  :page
  (fn [db _]
    (-> db :app :view :current)))

(rf/reg-sub
  :auth?
  (fn [db _]
    (-> db :auth)))

(rf/reg-sub
  :playlist/tracks
  (fn [db _]
    (-> db :tracklist)))