(ns frontend.state.events
  (:require
    [re-frame.core :as rf]
    [frontend.util :as util]
    [frontend.sente.init :refer [do-send-server]]
    [taoensso.timbre :as log]))

(declare request-token)
(declare save-token)

;;Events
(rf/reg-event-db
  :initialize
  (fn [_ _]
    {:ready true
     :app   {:view {:current :index}}}))

(rf/reg-event-fx
  :ready
  (fn [{:keys [db]} [_ open?]]
    {:db db}))

(rf/reg-event-fx
  :app
  (fn [{:keys [db]} [_ open?]]
    {:db db}))

(rf/reg-event-fx
  :websocket/open
  (fn [{:keys [db]} [_ open?]]
    {:db (assoc-in db [:app :websocket :state :open] true)}))

(rf/reg-event-fx
  :websocket/ready
  (fn [{:keys [db]} [_ [uid]]]
    {:db (assoc-in db [:app :websocket :state :uuid] uid)}))

(rf/reg-event-fx
  :local/store-token
  (fn [{:keys [db]} [_ token]]
    (save-token token)
    {:db db}))

(defn save-token [token]
  "Saves bearer token into local storage"
  (util/set-item! "Bearer" (str "Bearer " token)))

(rf/reg-event-fx
  :page
  (fn [{:keys [db]} [_ view]]
    {:db (assoc-in db [:app :view :current] (:handler view))}))

(rf/reg-event-fx
  :submit/search
  (fn [{:keys [db]} [_ formdata]]
    {:dispatch [:submit/send-form formdata]
     :db (assoc-in db [:form :data] formdata)}))

(rf/reg-event-fx
  :submit/send-form
  (fn [{:keys [db]} [_ formdata]]
    (do-send-server {:event-id :submit/send-form :args formdata})
    {:db db}))

(rf/reg-event-fx
  :submit/req-token
  (fn [{:keys [db]} _]
    (do-send-server {:event-id :submit/req-token})
    {:db db}))

(rf/reg-event-fx
  :submit/received-sugguestions
  (fn [{:keys [db]} [_ data]]
    (let [json->map (->> data
                     (.parse js/JSON)
                     (js->clj))
          items   (-> json->map
                   (get "tracks")
                   (get "items"))
          res []]
      {:db (assoc-in db [:tracklist]
             (map
               #(conj res {:id     (get % "id")
                           :href   (get % "href")
                           :name   (get % "name")
                           :images (-> %
                                     (get "album")
                                     (get "images"))})
               items))})))

(rf/reg-event-fx
  :auth!
  (fn [{:keys [db]} [_ _]]
    {:db (assoc db :auth true)}))

(rf/reg-event-fx
  :submit/add-to-playlist
  (fn [{:keys [db]} [_ track-id]]
    (do-send-server {:event-id :submit/add-to-playlist :args track-id})
    {:db db}))


