(ns backend.init
  (:require
    [clojure.tools.cli :refer [parse-opts]]
    [clojure.java.io :as io]
    [schema.core :as s]
    [taoensso.timbre :as log]
    [com.stuartsierra.component :as component]
    ;; imports
    [backend.components.database :refer [new-database]]
    [backend.components.api :refer [new-api]]
    [backend.components.socket :refer [new-socket]]
    [backend.components.router :refer [new-router]]
    [backend.components.webserver :refer [new-webserver]]))

(defonce system nil)

(defn system-map [{:keys [host port]} {:keys [user password db-port]}]
  (component/system-map
    :database (new-database user password host db-port)
    :api (new-api)
    :socket (new-socket)
    :router (component/using
              (new-router)
              [:socket :api])
    :http (component/using
            (new-webserver host port )
            [:router])))

(defn init [webserver database]
  (alter-var-root #'system
                  (constantly (system-map webserver database))))

(defn start []
  (alter-var-root #'system
                  (fn [s] (component/start s))))

(defn stop []
  (alter-var-root #'system
                  (fn [s] (when s (component/stop s)))))

(defn bootstrap
  ([]
   (init {:port 3000 :host "localhost"} {})
   (start))
  ([{:keys [server database]}]
   (init server database)
   (start)))

(def Config
  "Validates app configuration"
  {:server   {:port s/Num :host s/Str}
   :database {(s/optional-key :user)     s/Str
              (s/optional-key :password) s/Str
              (s/optional-key :db-port) s/Num}})

(defn load-cfg []
  (clojure.edn/read-string (slurp (io/resource "cfg.edn"))))

(defn -main []
  (let [config (load-cfg)]
    (if (s/validate Config config)
      (do
        (log/info "start up config => " config)
        (bootstrap config))
      (do
        (log/info "start up config => default")
        (bootstrap)))))
