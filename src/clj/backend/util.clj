(ns backend.util
  (:import java.util.Base64))

(defn encode-64 [to-encode]
  (.encodeToString (Base64/getEncoder) (.getBytes to-encode)))

(defn decode-64 [to-decode]
  (String. (.decode (Base64/getDecoder) to-decode)))