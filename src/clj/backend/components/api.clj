(ns backend.components.api
  (:require
    [taoensso.timbre :as log]
    [clj-http.client :as client]
    [ring.util.response :as resp]
    [ring.util.codec :as url]
    [schema.core :as s]
    [compojure.api.sweet :refer :all]
    [ring.util.http-response :refer [ok]]
    [com.stuartsierra.component :as component]))

(s/defschema Login
  {:code s/Any
   :state s/Any
   (s/optional-key :error) s/Any})

(defrecord Api []

  component/Lifecycle

  (start [this]
    (log/info "starting swagger-api...")
    (let [swagger-api
          (api
            {:swagger
             {:ui   "/api-docs"
              :spec "/swagger.json"
              :data {:info     {:title       "Swagger"
                                :description "Swagger test"}
                     :tags     [{:name "api" :description "some api"}]
                     :consumes ["application/json"]
                     :produces ["application/json"]}}}

            (context "/api" []
              :tags ["api"]

              (GET "/login" []
                :query-params [client_id :- s/Any, redirect_uri :- s/Any, scope :- s/Any]
                :summary "Authenticates user"
                (let [base     "https://accounts.spotify.com/authorize"
                      type     "?response_type=code"
                      client   (str "&client_id=" client_id)
                      scopes   (str "&scope=" (url/url-encode scope))
                      redirect (str "&redirect_uri=" (url/url-encode "http://localhost:3000/auth"))]
                  (ok)
                  (resp/redirect (str base type client scopes redirect))))))]

      (-> this
        (assoc :api swagger-api))))

  (stop [this]
    (log/info "swagger-api stopped...")
    (-> this
      (assoc :api nil))))

(defn new-api []
  (map->Api {}))