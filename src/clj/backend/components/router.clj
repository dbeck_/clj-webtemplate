(ns backend.components.router
  (:require
    [taoensso.timbre :as log]
    [clojure.pprint :as pp]
    [backend.util :refer [encode-64 decode-64]]
    [cheshire.core :refer [parse-string encode]]
    [compojure.handler :refer [site]]
    [com.stuartsierra.component :as component]
    [taoensso.sente :refer [start-server-chsk-router!]]
    [clojure.core.async :refer [<! >! put! close! go]]
    [backend.view.index :refer [landing-page]]
    [ring.middleware.anti-forgery :refer [wrap-anti-forgery]]
    [clj-http.client :as client]
    [ring.util.response :as resp])
  (:use
    (compojure [core :only [wrap-routes context routes defroutes GET POST]])))

(def playlist "1hx9jJ81bYebgj0lnRrtJf")
(defonce sente (atom nil))

(defonce oAuth (atom nil))
(add-watch oAuth :token
  (fn [_ _ old new]
    (when (not= old new)
      (println "Connected uids change: %s" new))))

;; TODO FOR DEVELOPMENT - REMOVE LATER
(defonce client-secret
  (let [cs {:secret "3ee520907145482d9f97d9e301a2cfc5"
            :client "c42cfca4e8d74c8eb6e72812f968379a"}]
    (encode-64 (str (:client cs) ":" (:secret cs)))))

(defn get-token
 "Requests bearer token from spotify endpoint to validate further requests. Requires
  Base64 encoded combination of a secret and a clientId. The token will then be written
  into the users browser storage."
  ([]
   (let [req (future (client/post "https://accounts.spotify.com/api/token"
                       {:form-params {:grant_type "client_credentials"}
                        :headers     {"Authorization" (str "Basic " client-secret)}}))]
     (if (and
           (= 200 (:status @req))
           (nil? (:val oAuth)))
       (swap! oAuth assoc :token (parse-string (-> @req :body)))
       (log/warn "Token allready set"))))
  ([code]
   (let [req (future (client/post "https://accounts.spotify.com/api/token"
                       {:form-params {:grant_type "authorization_code"
                                      :code code
                                      :redirect_uri "http://localhost:3000/auth"}
                        :headers     {"Authorization" (str "Basic " client-secret)}}))]
     (if (and
           (= 200 (:status @req))
           (nil? (:val oAuth)))
       (let [json->clj (parse-string (:body @req))]
         {:token         (-> json->clj (get "access_token"))
          :refresh-token (-> json->clj (get "refresh_token"))
          :scope         (-> json->clj (get "scope"))})
       (log/info "Token allready set")))))

(defn query-for-track
  "Queries spotifys search endpoint for entries that contain either a given artist or track"
  [{:keys [artist track]}]
  (try
    (let [query-params (str "q=" artist "*")
          req (future (client/get (str "https://api.spotify.com/v1/search?" query-params)
                        {:query-params {:type "track"
                                        :limit 15}
                         :oauth-token  (-> @oAuth
                                         first
                                         :session
                                         :token)}))]
      (log/info (-> @req :body))
      {:resp (:body @req)})
    (catch Exception e
      (log/info e))))

(defn get-track
  "Queries spotifys track endpoint for a specific track identified by its id"
  [id]
  (let [req (future (client/get (str "https://api.spotify.com/v1/tracks/" id)
                      {:oauth-token (-> @oAuth
                                      first
                                      :session
                                      :token)}))]
    {:resp (:body @req)}))

(defn add-track-to-playlist
  [id]
  (let [req (future (client/post (str "https://api.spotify.com/v1/playlists/" playlist "/tracks")
                      {:body (encode {:uris [id]})
                       :oauth-token  (-> @oAuth
                                       first
                                       :session
                                       :token)}))]
    {:resp (:body @req)}))

(defmulti event-msg-handler :id)

(defmethod event-msg-handler
  :default
  [{:as ev-msg :keys [event id ?data ring-req ?reply-fn send-fn]}]
  (when ?reply-fn
    (?reply-fn {:umatched-event-as-echoed-from-server event})))

(defmethod event-msg-handler
  :chsk/ws-ping
  [{:keys [event uid id ?data ring-req]}]
  (log/info "Client " uid " alive..."))

(defmethod event-msg-handler
  :submit/add-to-playlist
  [{:keys [event uid id ?data send-fn args]}]
  (let [[ev-id _] event
        spotify-uri (-> (get-track ?data)
                      :resp
                      parse-string
                      (get "uri"))]
    (try
      (println (get-track ?data))
      (add-track-to-playlist spotify-uri)
      #_(send-fn uid [ev-id {:?data
                           (get-track ?data)}])
      (catch Exception e
        (log/error e)))))

(defmethod event-msg-handler
  :submit/req-token
  [{:keys [event uid id ?data send-fn]}]
  (let [[ev-id _] event]
    (try
      (let [token (future (get-token))]
        (send-fn uid [ev-id {:?data
                             (-> @token
                               :token
                               (get "access_token"))}]))
      (catch Exception e
        (log/error e)))))

(defmethod event-msg-handler
  :submit/send-form
  [{:keys [event uid id ?data ring-req ?reply-fn send-fn]}]
  (let [[ev-id _] event]
    (send-fn uid [ev-id {:?data (-> (query-for-track ?data)
                                     :resp)}])))

(defroutes
  app
  ;; auth
  (GET "/auth" [req] (fn [req]
                       (let [auth-code (-> req :params :code)]
                         (swap! oAuth conj {:session (get-token auth-code)})
                         (resp/redirect "http://localhost:3000"))))
  ;; Base routes
  (GET "/" req (landing-page req))
  ;; Websocket
  (GET "/chsk" [req] (fn [req] ((get-in @sente [:websocket :get]) req)))
  (POST "/chsk" [req] (fn [req] ((get-in @sente [:websocket :post]) req))))

(defrecord Router [socket api]

  component/Lifecycle

  (start [this]
    (log/info "starting sente...")
    (let [{:keys [chsk ajax-post connected?
                  ajax-get send!]}
          (-> socket :websocket)
          swagger (-> api :api)]
      (swap! sente assoc
        :websocket {:get       ajax-get
                    :post      ajax-post
                    :to-client send!
                    :clients   connected?})
      (start-server-chsk-router! chsk event-msg-handler)
      (-> this
        (assoc :routes (site
                         (routes
                           app
                           swagger))))))
  (stop [this]
    (log/info "stop routing...")
    (-> this
      (assoc :routes nil))
    this))

(defn new-router []
  (map->Router {}))
