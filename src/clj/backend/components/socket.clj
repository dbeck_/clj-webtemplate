(ns backend.components.socket
  (:require
    [taoensso.timbre :as log]
    [taoensso.sente.server-adapters.http-kit :refer (get-sch-adapter)]
    [clojure.core.async :refer [<! >! put! close! go]]
    [com.stuartsierra.component :as component]
    [taoensso.sente :as sente]))

(defrecord Socket []

  component/Lifecycle

  (start [this]
    (log/info "starting websocket...")
    (let [connection (sente/make-channel-socket! (get-sch-adapter)
                       {:user-id-fn (fn [ring-req]
                                      (get-in ring-req [:params :client-id]))})
          sente      {:ajax-post  (:ajax-post-fn connection)
                      :ajax-get   (:ajax-get-or-ws-handshake-fn connection)
                      :chsk       (:ch-recv connection)
                      :send!      (:send-fn connection)
                      :connected? (:connected-uids connection)}]
      (-> this
          (assoc :websocket sente))))

  (stop [this]
    (log/info "websocket stopped...")
    (-> this
        (assoc :websocket nil))))

(defn new-socket []
  (map->Socket {}))