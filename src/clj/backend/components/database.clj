(ns backend.components.database
  (:require [com.stuartsierra.component :as component]
            [clojure.java.jdbc :as sql]
            [taoensso.timbre :as log]))

(def db (atom {}))

(defn with-db [query]
  (sql/query (-> @db :conn) query))

;; Queries ------------------------------------------

;;

(defrecord database [user password host port]

  component/Lifecycle

  (start [this]
    (log/info "starting db connection...")
    (let [connection
          {:dbtype   "postgresql"
           :dbname   "postgres"
           :host     "localhost"
           :user     "postgres"
           :password "1234"}]
      (swap! db assoc :conn connection)
      (-> this
        (assoc :db connection))))

  (stop [this]
    (log/info "closing db connection...")
    (swap! db assoc :conn nil)
    (-> this
      (assoc :db nil))))

(defn new-database [user password host port]
  (map->database {:user     user
                  :password password
                  :host     host
                  :port     port}))