(ns backend.components.webserver
  (:require
    [taoensso.timbre :as log]
    [ring.middleware.anti-forgery :refer [wrap-anti-forgery]]
    [ring.middleware.session :refer :all]
    [com.stuartsierra.component :as component]
    [ring.middleware.resource :refer [wrap-resource]])
  (:use
    [org.httpkit.server :only (run-server)]
    (compojure [handler :only [site]])))

(defn stop-server [webserver]
  (when-not (nil? @webserver)
    (@webserver :timeout 100)
    (reset! webserver nil)))

(defrecord Webserver [router host port]

  component/Lifecycle

  (start [this]
    (let [server (atom {})
          routes (:routes router)]
      (try
        (log/info "serving app on port =>" port)
        (reset!
          server (run-server
                   (-> routes
                     (wrap-anti-forgery)
                     (wrap-session)
                     (wrap-resource "public"))
                   {:port port}))
        (catch Exception e
          (log/error (.getMessage e)
            {:status :failed
             :host   host
             :port   port})))
      (-> this
        (assoc :server server))))

  (stop [this]
    (log/info "closing port =>" port)
    (stop-server (:server this))
    (-> this
      (assoc :server nil))))

(defn new-webserver [host port]
  (map->Webserver {:host host :port port}))
