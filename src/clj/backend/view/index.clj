(ns backend.view.index
  (:require [hiccup.core :as h]
            [ring.middleware.anti-forgery :as anti-forgery]))

(defn landing-page [req]
  (h/html
    [:head
     [:meta {:charset "UTF-8"}]
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
     [:title "Demo Application"]
     [:link {:rel "stylesheet" :href "/css/materialize.css"}]
     [:style {:id "_stylefy-constant-styles_"}]
     [:style {:id "_stylefy-styles_"}]]
    [:body
     [:div#app
      (let [csrf-token (force anti-forgery/*anti-forgery-token*)]
        [:div#sente-csrf-token {:data-csrf-token csrf-token} "In order to work properly, this page requires JavaScript."])
      [:script {:src "/js/main.js"}]
      [:script {:src "https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"}]
      [:script "frontend.core.init()"]]]))