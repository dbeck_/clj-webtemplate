(defproject clj-webtemplate "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :plugins [[lein-cljsbuild "1.1.7"]]
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [stylefy "1.13.3"]
                 [clj-http "3.10.0"]
                 [org.clojure/tools.cli "0.4.2"]
                 [re-frame "0.11.0-rc1"]
                 [org.clojure/core.async "0.4.490"]
                 [org.clojure/clojurescript "1.10.520"]
                 [com.taoensso/encore "2.112.0"]
                 [com.taoensso/timbre "4.10.0"]
                 [com.cognitect/transit-clj "0.8.313"]
                 [com.stuartsierra/component "0.4.0"]
                 [http-kit "2.3.0"]
                 [com.taoensso/sente "1.14.0-RC2"]
                 [jarohen/chord "0.8.1"]
                 [hiccup "1.0.5"]
                 [org.postgresql/postgresql "42.2.6.jre7"]
                 [compojure "1.6.1"]
                 [ring "1.7.0"]
                 [com.bhauman/figwheel-main "0.2.0"]
                 [com.bhauman/rebel-readline-cljs "0.1.4"]
                 [figwheel-sidecar "0.5.16"]
                 [prismatic/schema "1.1.11"]
                 [metosin/compojure-api "2.0.0-alpha30"]
                 [binaryage/devtools "0.9.10"]
                 [reagent "0.8.1"]
                 [bidi "2.1.6"]
                 [kibu/pushy "0.3.8"]
                 [org.clojure/java.jdbc "0.7.9"]
                 [cljs-http "0.1.46"]
                 [reagent-utils "0.3.2"]
                 [cider/piggieback "0.3.8"]]

  :cljsbuild
  {:builds
   {:min
    {:source-paths ["src/cljs"]
     :compiler {:main frontend.core
                :output-to "target/public/cljs-out/app.js"
                :asset-path "target/public/cljs-out/app"
                :optimizations :simple
                :pretty-print false}}}}

  :main ^:skip-aot backend.init
  :source-paths ["src/clj" "src/cljs"]
  :resource-paths ["target" "resources"]
  :clean-targets ^{:protect false} ["target"]
  :aliases {"build" ["trampoline" "run" "-m" "figwheel.main"]
            "fig" ["trampoline" "run" "-m" "figwheel.main" "-b" "dev" "-r"]}

  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
